function ligarnota(){
    document.getElementById("nota").src="notagif.gif";
}
function mouseOver() {
    document.getElementById("mouseAlt").innerHTML = "Gerar Notas"
}
function mouseOut() {
    document.getElementById("mouseAlt").innerHTML = "Turma A"
}
function mouseOver1() {
    document.getElementById("mouseAlt1").innerHTML = "Gerar Notas"
}
function mouseOut1() {
    document.getElementById("mouseAlt1").innerHTML = "Turma B"
}
function chamaonFocus(){
    alert("Insira os dados do Aluno");
    return document.onblur
}
function chamaonblur(){
    alert("Insira a nota do aluno");
    return document.chamaonblur
}

let linhasTabela = [
["Rafael", 8.2, 6.6, 4.5, 4.5, 5.95, "recuperação"],
["Joao", 5.3, 7.2, 7.2, 7.2, 6.725, "recuperação"],
["Afonso", 4.5, 10, 8.2, 8.2, 7.725, "aprovado"],
["Teo", 6.5, 6.5, 7.5, 7.5, 7, "aprovado"],
["Alana", 9.5, 4.5, 10, 10, 8.5, "aprovado"],
["Bia", 8.2, 10, 5.3, 5.3, 7.2, "aprovado"],
["Julia", 10, 5.3, 9.5, 9.5, 8.575, "aprovado"],
["Margo", 7.5, 4.5, 7.5, 7.5, 6.75, "aprovado"],
["Plinio", 6.6, 3.5, 8.2, 8.2, 6.625, "recuperação"],
["Carlos", 3.5, 8.6, 9.2, 9.2, 7.625, "aprovado"],
["Pedro", 4.5, 9.2, 4.5, 4.5, 5.675, "recuperação"],
["Joseph", 10, 7.5, 10, 10, 9.375, "aprovado"],
["Julia", 9.2, 8.2, 6.5, 6.5, 7.6, "aprovado"],
["Carla", 8.6, 5, 6.6, 6.6, 6.7, "recuperação"],
["Alexia", 7.5, 9.5, 3.5, 3.5, 6, "recuperação"],
["Tina", 7.2, 7.5, 8.6, 8.6, 7.975, "aprovado"],
["Naty", 5, 8.2, 5, 5, 5.8, "recuperação"],
]

function criaTag(elemento) {
return document.createElement(elemento)
}

let titulo = document.querySelector("section h1");
titulo.textContent = "NOTA DOS ALUNOS";

let tabela = document.getElementById("tabela");

let thead = criaTag("thead");
let tbody = criaTag("tbody");
let tfoot = criaTag("tfoot");

let indicesTabela = ["nome", "Nota 1", "Nota 2", "Nota 3", "Nota 4", "media", "estado"];
let linhaHead = criaTag("tr");

function criaCelula(tag, text) {
    tag = criaTag(tag);
    tag.textContent = text;
    return tag;
}

for(j = 0; j < indicesTabela.length; j++) {
    let th = criaCelula("th", indicesTabela [j]);
    linhaHead.appendChild(th);
}
thead.appendChild(linhaHead);


for(j = 0; j < linhasTabela.length; j++) {
    let linhaBody = criaTag("tr");

    for(i = 0; i < linhasTabela[j].length; i++) {
        cel = criaCelula("td", linhasTabela[j][i]);
        linhaBody.appendChild(cel); 
    }
    tbody.appendChild(linhaBody);
}

tabela.appendChild(thead);
tabela.appendChild(tbody);
tabela.appendChild(tfoot);

